const createFlipstarterCampaignSite = require("./src/createFlipstarterCampaignSite")
const createFlipstarterCampaignHtml = require('./src/createFlipstarterCampaignHtml')
const createFlipstarterCampaignBlobUrl  = require('./src/createFlipstarterCampaignBlobUrl')
const parseMarkdown = require("./src/markdown");

module.exports = {
  createFlipstarterCampaignSite,
  createFlipstarterCampaignHtml,
  createFlipstarterCampaignBlobUrl,
  parseMarkdown
}