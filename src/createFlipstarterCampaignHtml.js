const SATS_PER_BCH = 100000000
const moment = require('moment');
const mustache = require('mustache');
const parseMarkdown = require('./markdown')

module.exports = async function createFlipstarterCampaignHtml(template, campaign, preview = false) {

  const today_date = moment().unix()

  let timerLabel, timerDaysUntil
  
  //for deterministic output
  if (!preview) {
    
    timerLabel = "Loading..."
    timerDaysUntil = ""

  } else if (today_date < campaign.starts) {

    timerLabel = "Starts"
    timerDaysUntil = moment().to(moment.unix(campaign.starts))

  } else {
    
    timerLabel = today_date < campaign.expires ? "Expires" : "Expired"
    timerDaysUntil = moment().to(moment.unix(campaign.expires))
  }

  const requestedAmountInSatoshis = campaign.recipients.reduce((sum, recipient) => sum + recipient.satoshis, 0)
  const requestedAmount = Number((requestedAmountInSatoshis / SATS_PER_BCH).toFixed(8));

  return mustache.render(template, {
    title: campaign.title,
    description: await parseMarkdown(campaign.descriptions.en.abstract),
    details: await parseMarkdown(campaign.descriptions.en.proposal),
    url: campaign.recipients[0].url,
    image: campaign.recipients[0].image,
    recipients: campaign.recipients.map(recipient => {
      return {
        ...recipient,
        amountInSatoshis: recipient.satoshis,
        amountInBch: Number((recipient.satoshis / SATS_PER_BCH).toFixed(8))
      }
    }),
    requestedAmount,
    recipientCount: campaign.recipients.length,
    timerLabel,
    timerDaysUntil,
    contributorsLabel: "contributors",
    recipientsLabel: "recipients"
  })
}