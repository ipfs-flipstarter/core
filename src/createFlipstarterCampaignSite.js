const { createLink, createNode, encode } = require('@ipld/dag-pb')
const all = require("it-all")

/**
 * 
 * @param {import('ipfs').IPFS} ipfs 
 * @param {*} indexPageHtml 
 * @param {*} initialCampaign 
 * @returns 
 */
module.exports = async function createFlipstarterCampaignSite(ipfs, baseClientCid, indexPageHtml, initialCampaign) {  
  const [indexPageLink, campaignLink] = await Promise.all([
    uploadFile(ipfs, "index.html", indexPageHtml),
    uploadFile(ipfs, "campaign.json", JSON.stringify(initialCampaign))
  ])

  return await uploadSite(ipfs, baseClientCid, indexPageLink, campaignLink)
}

/**
 * 
 * @param {import('ipfs').IPFS} ipfs 
 * @param {*} name 
 * @param {*} data 
 * @returns 
 */
async function uploadFile(ipfs, name, data) {
	const { cid, size } = await ipfs.add(data);
	return createLink(name, size, cid);
}

/**
 * 
 * @param {import('ipfs').IPFS} ipfs 
 * @param {import('@ipld/dag-pb').PBNode} clientDag
 * @param {*} indexLink 
 * @param {*} campaignLink 
 * @returns 
 */
async function uploadSite(ipfs, clientDag, indexLink, campaignLink) {   
  const clientLinks = clientDag.Links.filter((link) => link.Name !== 'campaign.json' && link.Name !== 'index.html');
  const clientDagNode = createNode(clientDag.Data, [ ...clientLinks, campaignLink, indexLink ]);
  
  return await ipfs.dag.put(encode(clientDagNode), { 
    storeCodec: 'dag-pb',
    inputCodec: 'dag-pb',
    //Backwards-compat 
    format: 'dag-pb'
  });
}