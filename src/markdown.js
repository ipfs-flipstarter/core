// Print out the campaign texts.
let processor = null;
async function getProcessor() {
  if (!processor) {
    const [
      { unified },
      { default: markdown },
      { default: remark2rehype },
      { default: githubMarkdown },
      { default: html },
      { default: htmlInMarkdown },
    ] = await Promise.all([
      import("unified"),
      import('remark-parse'),
      import('remark-rehype'),
      import('remark-gfm'),
      import('rehype-stringify'),
      import('rehype-raw'),
    ]);

    processor = unified()
      .use(githubMarkdown)
      .use(markdown)
      .use(remark2rehype, {allowDangerousHtml: true})
      .use(htmlInMarkdown)
      .use(html)
  }

  return processor;
}

module.exports = async function parseMarkdown(content) {
    const processor = await getProcessor();
    const result = await processor.process(content)
    return String(result);
}